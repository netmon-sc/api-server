<?php

namespace Netmon\Server\App\Transformers;

use League\Fractal\TransformerAbstract;

class ResourceTransformer extends TransformerAbstract
{
    /**
     * List of resources possible to include
     *
     * @var  array
     */
    protected $availableIncludes = [];

    /**
     * List of resources to automatically include
     *
     * @var  array
     */
    protected $defaultIncludes = [
        'actions'
    ];

    /**
     * Transform object into a generic array
     *
     * @var  object
     */
    public function transform($resource)
    {
        return [
            'id' => $resource['name'],
            'name' => $resource['name'],
        ];
    }

    /**
     * Include Author
     *
     * @param Book $book
     * @return \League\Fractal\Resource\Item
     */
    public function includeActions($resource)
    {
        return $this->collection($resource['actions'], new ActionTransformer, 'actions');
    }
}
