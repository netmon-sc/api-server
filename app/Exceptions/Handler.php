<?php

namespace Netmon\Server\App\Exceptions;

use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [

    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
        return parent::render($request, $exception);

        // if the clients wants json, then use JsonApi ErrorHandler
//        if($request->wantsJson() || $request->ajax()) {
            $errorHandler = \App::make(\Tobscure\JsonApi\ErrorHandler::class);
            $response = $errorHandler->handle($exception);
            $document = new \Tobscure\JsonApi\Document();
            $document->setErrors($response->getErrors());
            return response($document, $response->getStatus());
//        }

        // use default error handling otherwise
//        return parent::render($request, $exception);
    }
}
