<?php

namespace Netmon\Server\App\Providers;

use Illuminate\Support\ServiceProvider;

class ModulesServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        try {
            $modules = \DB::table('modules')->select('name', 'provider', 'enabled')->get();

            //register service providers of all installed netmon modules
            foreach($modules as $module) {
                if($module->enabled)
                    $this->app->register($module->provider);
            }
        } catch(\Exception $e) {
            \Log::info("Modules have been disabled due to ".$e->getMessage());
        }
    }
}
