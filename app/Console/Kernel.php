<?php

namespace Netmon\Server\App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        \Netmon\Server\App\Console\Commands\Inspire::class,
        \Netmon\Server\App\Console\Commands\ModuleInstall::class,
        \Netmon\Server\App\Console\Commands\ModuleConfigure::class,
        \Netmon\Server\App\Console\Commands\ModuleDisable::class,
        \Netmon\Server\App\Console\Commands\ModulesUpdate::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('inspire')
                 ->hourly();
    }

    protected function commands() {
        $this->load(__DIR__.'/Commands');
    }
}
