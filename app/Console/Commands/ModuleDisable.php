<?php

namespace Netmon\Server\App\Console\Commands;

use Illuminate\Console\Command;
use ApiServer\Modules\Models\Module;
use Artisan;

class ModuleDisable extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'module:disable {name}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Disables a module';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            $module = Module::where('name', '=', $this->argument('name'))->firstOrFail();
        } catch(\Exception $e) {
            $this->info("Module not found.");
        }

        if($module->enabled) {
            $module->enabled = false;
            $module->save();
            $this->info("Module ".$this->argument('name')." disabled.");
        } else {
            $this->info("Module ".$this->argument('name')." already disabled.");
        }
    }
}
