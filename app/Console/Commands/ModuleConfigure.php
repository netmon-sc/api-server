<?php

namespace Netmon\Server\App\Console\Commands;

use Illuminate\Console\Command;
use ApiServer\Modules\Models\Module;
use Artisan;

class ModuleConfigure extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'module:configure {name}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Configurates a module';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // check if module exists
        try {
            $module = Module::where('name', '=', $this->argument('name'))->firstOrFail();
        } catch(\Exception $e) {
            $this->info("Module ".$this->argument('name')." not found. This is default on new installations.");
            $this->info("Please configure modules manually.");
            return;
        }

        $this->info("Publishing module files");
        $exitCode = Artisan::call('vendor:publish', [
            '--provider' => $module->provider
        ]);

        $this->info("Running migrations");
        $exitCode = Artisan::call('migrate', [
            '--force' => true
        ]);
    }
}
