<?php

namespace Netmon\Server\App\Console\Commands;

use Illuminate\Console\Command;
use ApiServer\Modules\Models\Module;
use Artisan;

class ModuleInstall extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'module:install {name} {provider}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Installs a Netmon module.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // dont install modules if this is a clean installation
        if(!\Schema::hasTable('modules')) {
            $this->info("The modules table does not exist. This is default on new installations.");
            $this->info("Please install modules manually.");
            return;
        }

        // dont install module if it is already installed
        $modules = Module::where(
            'provider', '=', $this->argument('provider')
        )->get();
        if($modules->count()>0) {
            $this->info("Module ".$this->argument('name')." is already installed");
            return;
        }

        if(class_exists($this->argument('provider'))) {
            $module = Module::create([
                'name' => $this->argument('name'),
                'provider' => $this->argument('provider'),
                'enabled' => true
            ]);
            $this->info("Module ".$this->argument('name')." installed successfully");
        } else {
            $this->error(
                "The service provider ".addslashes($this->argument('provider'))
                ." for the Module".$this->argument('name')." cannot be loaded."
            );
        }
    }
}
