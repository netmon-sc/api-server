<?php

namespace Netmon\Server\App\Console\Commands;

use Illuminate\Console\Command;
use ApiServer\Modules\Models\Module;
use Artisan;

class ModulesUpdate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'modules:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Updates all modules';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // check if module exists
        $modules = Module::get();

        foreach($modules as $module) {
            $this->info("Updating module ".$module->name);
            $this->info("Publishing module files");
            $exitCode = Artisan::call('vendor:publish', [
                '--provider' => $module->provider
            ]);

            $this->info("Running migrations");
            $exitCode = Artisan::call('migrate', [
                '--force' => true
            ]);
        }

        $this->info("All updates finished");
    }
}
