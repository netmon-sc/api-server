<?php

namespace Netmon\Server\App\Http\Middleware;

use Auth;
use Closure;

class AuthenticateOnceWithBasicAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $authenticationFailed = Auth::onceBasic('name');
        if($authenticationFailed) {
            abort(401, trans('auth.failed'));
        }
        return $next($request);
    }

}
