<?php

namespace Netmon\Server\App\Http\Controllers;

use Netmon\JsonApi\Http\Controllers\DefaultResourceController;

class ConfigsController extends DefaultResourceController
{
    public function model() {
      return \Netmon\Server\App\Models\Config::class;
    }

    public function serializer() {
        return \Netmon\Server\App\Serializers\ConfigSerializer::class;
    }

    public function resource() {
        return "configs";
    }
}
