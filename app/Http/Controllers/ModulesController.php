<?php

namespace Netmon\Server\App\Http\Controllers;

use Netmon\JsonApi\Http\Controllers\DefaultResourceController;

class ModulesController extends DefaultResourceController
{
    public function model() {
      return \Netmon\Server\App\Models\Module::class;
    }

    public function serializer() {
        return \Netmon\Server\App\Serializers\ModuleSerializer::class;
    }

    public function resource() {
        return "modules";
    }
}
