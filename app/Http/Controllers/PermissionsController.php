<?php namespace Netmon\Server\App\Http\Controllers;

use Netmon\JsonApi\Http\Controllers\DefaultResourceController;

class PermissionsController extends DefaultResourceController
{
    public function model() {
      return \Netmon\Server\App\Models\Permission::class;
    }

    public function serializer() {
        return \Netmon\Server\App\Serializers\PermissionSerializer::class;
    }

    public function resource() {
        return "permissions";
    }
}
