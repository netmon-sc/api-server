<?php

namespace Netmon\Server\App\Http\Controllers;

use Netmon\Server\App\Exceptions\Exceptions\UnauthorizedException;

use Netmon\JsonApi\Http\Controllers\DocumentResourceController;

class MeController extends DocumentResourceController
{
    public function model() {
      return \Netmon\Server\App\Models\User::class;
    }

    public function serializer() {
        return \Netmon\Server\App\Serializers\UserSerializer::class;
    }

    public function resource() {
        return "me";
    }

    /**
     * Show
     *
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $user = \Auth::user();
        if (empty($user->id)) {
            throw new UnauthorizedException(trans('api.unauthenticated'));
        }

        $user->load($this->includes);

        //return new document
        return $this->documentResponse($user);
    }
}
