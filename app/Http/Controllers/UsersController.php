<?php

namespace Netmon\Server\App\Http\Controllers;

use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\ConflictHttpException;
use Symfony\Component\HttpKernel\Exception\GoneHttpException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\LengthRequiredHttpException;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Symfony\Component\HttpKernel\Exception\NotAcceptableHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\PreconditionFailedHttpException;
use Symfony\Component\HttpKernel\Exception\PreconditionRequiredHttpException;
use Symfony\Component\HttpKernel\Exception\ServiceUnavailableHttpException;
use Symfony\Component\HttpKernel\Exception\TooManyRequestsHttpException;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Symfony\Component\HttpKernel\Exception\UnsupportedMediaTypeHttpException;

use Illuminate\Database\Eloquent\ModelNotFoundException;

use Illuminate\Http\Request;
use Netmon\Server\App\Models\User;
use Netmon\Server\App\Models\Role;
use Netmon\Server\App\Models\Config;
use Netmon\Server\App\Transformers\UserTransformer;
use Netmon\Server\App\Mail\ResetPassword;


use Auth;
use Exception;
use DB;
use Gate;

use Tobscure\JsonApi\Document;
use Tobscure\JsonApi\Collection;
use Tobscure\JsonApi\Resource;
use Netmon\Server\App\Serializers\UserSerializer;

use Netmon\JsonApi\Http\Controllers\DefaultResourceController;

/**
 * User resource representation.
 *
 * @Resource("Users", uri="/users")
 */
class UsersController extends DefaultResourceController
{
    public function model() {
      return \Netmon\Server\App\Models\User::class;
    }

    public function serializer() {
        return \Netmon\Server\App\Serializers\UserSerializer::class;
    }

    public function resource() {
        return "users";
    }

    public function signup(Request $request) {
        \DB::beginTransaction();
        try {
            $user = new User();
            $user->fill($request->input());
            $user->save();

            //If this is the first user beeing created then the user will get
            //the admin role assigned
            $userlist = User::get();
            if(count($userlist) == 1) {
                $user->attach(
                    Role::findOrFail(
                        Config::where(
                            'key', 'serverAdminRoleId'
                        )->firstOrFail()->value
                    )
                );
            } else {
                // assign user role
                $user->attach(
                    Role::findOrFail(
                        Config::where(
                            'key', 'serverUserRoleId'
                        )->firstOrFail()->value
                    )
                );
            }
        } catch(\Exception $e) {
            DB::rollBack();
            throw $e;
        }
        DB::commit();

        //on success: return empty response with code 201
        return response()->created();
    }

    public function initiatePasswordReset(Request $request) {
        $username = $request->input('name');
        try {
            $user = User::where('name', '=', $request->input('name'))->firstOrFail();
        } catch (ModelNotFoundException $e) {
            throw new NotFoundHttpException(trans(
                'api.password_reset_username_not_found', [
                    'username' => $username
                ])
            );
        }

        if(empty($user->email)) {
            throw new NotFoundHttpException(trans(
                'api.password_reset_no_email', [
                    'username' => $username
                ])
            );
        }

        //generate new jwt that expires in 30 min
        $resetTokenTTL = Config::where(
            'key', 'serverPasswordResetTokenTTL'
        )->firstOrFail()->value;

        $customClaims = ['exp' => time()+$resetTokenTTL];
        $authToken = \JWTAuth::customClaims($customClaims)->fromUser($user);

        //get url to a clients password reset webinterface
        $passwordRestUrl = Config::where(
            'key', 'serverPasswordResetURL'
        )->firstOrFail()->value;
        //send email
        \Mail::to($user)->send(
            new ResetPassword(
                $user,
                $passwordRestUrl,
                $authToken
            )
        );

        //on success: return empty response with code 201
        return response()->created();
    }
}
