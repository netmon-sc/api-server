<?php namespace Netmon\Server\App\Http\Controllers;

use Netmon\JsonApi\Http\Controllers\DefaultResourceController;

class RolesController extends DefaultResourceController
{
    public function model() {
      return \Netmon\Server\App\Models\Role::class;
    }

    public function serializer() {
        return \Netmon\Server\App\Serializers\RoleSerializer::class;
    }

    public function resource() {
        return "roles";
    }
}
