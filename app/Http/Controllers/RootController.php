<?php

namespace Netmon\Server\App\Http\Controllers;

use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\ConflictHttpException;
use Symfony\Component\HttpKernel\Exception\GoneHttpException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\LengthRequiredHttpException;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Symfony\Component\HttpKernel\Exception\NotAcceptableHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\PreconditionFailedHttpException;
use Symfony\Component\HttpKernel\Exception\PreconditionRequiredHttpException;
use Symfony\Component\HttpKernel\Exception\ServiceUnavailableHttpException;
use Symfony\Component\HttpKernel\Exception\TooManyRequestsHttpException;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Symfony\Component\HttpKernel\Exception\UnsupportedMediaTypeHttpException;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Gate;
use Auth;

use Tobscure\JsonApi\Document;
use Tobscure\JsonApi\Collection;
use Tobscure\JsonApi\Resource;

use Netmon\JsonApi\Http\Controllers\DefaultResourceController;


class RootController extends Controller
{
    /**
     * Index
     *
     * Display a listing of the resource.
     *
     * @Get("/")
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Create a new JSON-API document with that collection as the data.
        $apiDocument = new Document();

        foreach (\Route::getRoutes()->getRoutes() as $route)
        {
            $controller = $route->getController();
            if($controller instanceof DefaultResourceController) {
                if(Gate::allows('index', $controller->model())) {
                    $apiDocument->addLink(
                        "{$controller->resource()}_index",
                        "TODO"
                    );
                }
            }
        }

        $apiDocument->addMeta('info', 'Take a look at the top level links attribute to see which resources are available for you. The collection of resources may change based on authorisation.');
    	//on success: return the newly created resource with statuscode 201
        return response()->json($apiDocument)->setStatusCode(200);
    }
}
