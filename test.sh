#! /bin/bash

php artisan migrate:refresh

# Creation of users
echo "create new user 'admin1'"
curl -H "Content-Type: application/json" -X POST -d '{"data": {"type": "user","attributes": {"name": "admin1","password": "testtest","email": null,"remember_token": null, "created_at": null, "updated_at": null}}}' http://localhost/netmonsc/core/public/users | jq
echo "create new user 'admin2'"
curl -H "Content-Type: application/json" -X POST -d '{"data": {"type": "user","attributes": {"name": "admin2","password": "testtest","email": null,"remember_token": null, "created_at": null, "updated_at": null}}}' http://localhost/netmonsc/core/public/users | jq
echo "create new user 'user1'"
curl -H "Content-Type: application/json" -X POST -d '{"data": {"type": "user","attributes": {"name": "user1","password": "testtest","email": null,"remember_token": null, "created_at": null, "updated_at": null}}}' http://localhost/netmonsc/core/public/users | jq
echo "create new user 'user2'"
curl -H "Content-Type: application/json" -X POST -d '{"data": {"type": "user","attributes": {"name": "user2","password": "testtest","email": null,"remember_token": null, "created_at": null, "updated_at": null}}}' http://localhost/netmonsc/core/public/users | jq
echo "show user 'admin1'"
curl -u 'admin1':'testtest' http://localhost/netmonsc/core/public/users/1 | jq

# Creation attaching, detaching and deleting roles
echo "create new role 'testrole'"
curl -u 'admin1':'testtest' -H "Content-Type: application/json" -X POST -d '{"data": {"type": "role", "id": "testrole", "attributes": {"description": null, "created_at": null, "updated_at": null}}}' http://localhost/netmonsc/core/public/roles | jq
echo "create new role 'deletedrole'"
curl -u 'admin1':'testtest' -H "Content-Type: application/json" -X POST -d '{"data": {"type": "role", "id": "deletedrole", "attributes": {"description": null, "created_at": null, "updated_at": null}}}' http://localhost/netmonsc/core/public/roles | jq
echo "list all roles"
curl -u 'admin1':'testtest' http://localhost/netmonsc/core/public/roles | jq
echo "attach roles 'admin', 'testrole' and 'deletedrole' to 'admin1'"
curl -u 'admin1':'testtest' -H "Content-Type: application/json" -X POST -d '{"data": [{ "type": "roles", "id": "admin" }, { "type": "roles", "id": "testrole" }, { "type": "roles", "id": "deletedrole" }]}' http://localhost/netmonsc/core/public/users/2/roles | jq
echo "show 'admin2'"
curl -u 'admin1':'testtest' http://localhost/netmonsc/core/public/users/2 | jq

echo "delete testrole from admin2, but authorisation is denied"
curl -u 'user1':'testtest' -X "DELETE" http://localhost/netmonsc/core/public/users/2/roles/testrole | jq
echo "delete testrole from admin2"
curl -u 'admin1':'testtest' -X "DELETE" http://localhost/netmonsc/core/public/users/2/roles/testrole
echo "delete 'deletedrole'"
curl -u 'admin1':'testtest' -X "DELETE" http://localhost/netmonsc/core/public/roles/deletedrole
echo "show 'admin2'"
curl -u 'admin1':'testtest' http://localhost/netmonsc/core/public/users/2 | jq

# Delete users
echo "user1 tries to delete user2 but is not allowed"
curl -u 'user1':'testtest' -X "DELETE" http://localhost/netmonsc/core/public/users/4
echo "user2 tries to delete himself"
curl -u 'user2':'testtest' -X "DELETE" http://localhost/netmonsc/core/public/users/4
echo "show non existing 'user2'"
curl -u 'admin1':'testtest' http://localhost/netmonsc/core/public/users/4 | jq

exit 0
