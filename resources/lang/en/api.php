<?php

return [
    "unauthorized" => "Authorisation failed.",
    "unauthenticated" => "Not authenticated.",
  "resource_not_found" => "The :modelName (:primaryKey) could not be found.",
  "unauthorized_resource" => "You are not authorized to :action :modelName ".
                             "(:primaryKey).",
  "unauthorized_resource_store" => "You are not authorized to store a :modelName.",
  "unauthorized_resource_index" => "You are not authorized to access the list of :modelName.",
  "unauthorized_attach" => "You are not authorized to attach :modelToAttach ".
                           "(:primaryKeyToAttach) to :modelToAttachTo ".
                           "(:primaryKeyToAttachTo).",
 "unauthorized_detach" => "You are not authorized to detach :modelToDetach ".
                          "(:primaryKeyToDetach) from :modelToDetachFrom ".
                          "(:primaryKeyToDetachFrom).",
  "destroy_failed" => "Could not delete :modelName (:primaryKey).",
  "update_failed" => "Could not update :modelName (:primaryKey).",
  "password_reset_username_not_found" => "The user \":username\" does not exist.",
  "password_reset_no_email" => "User \":username\" has no email set."

];

?>
