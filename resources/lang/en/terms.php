<?php

return [
  "user" => "user|users",
  "role" => "role|roles",
  "permission" => "permission|permissions",
  "device" => "devices|devices",

  "show" => "show",
  "index" => "list",
  "store" => "create",
  "update" => "update",
  "destroy" => "remove",

];

?>
