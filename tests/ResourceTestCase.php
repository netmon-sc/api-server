<?php

namespace Netmon\Server\Tests;

abstract class ResourceTestCase extends BasicTestCase
{

    public abstract function getStructure();

    public function getCollectionStructure() {
        return [
            'data' => [
                '*' => $this->getStructure()
            ]
        ];
    }

    public function getResourceStrucure() {
        return [
            'data' => $this->getStructure()
        ];
    }
}
