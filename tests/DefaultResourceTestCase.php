<?php

namespace Netmon\Server\Tests;

use Netmon\Server\App\Models\User;
use Netmon\Server\App\Models\Role;
use Netmon\Server\App\Models\Permission;

abstract class DefaultResourceTestCase extends ResourceTestCase
{
    /**
	 * Do this before each test
     */
	public function setUp() {
		parent::setUp();
    }
}
