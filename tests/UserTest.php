<?php

namespace Netmon\Server\Tests;

use JWTAuth;

use Netmon\Server\App\Models\User;
use Netmon\Server\App\Models\Role;
use Netmon\Server\App\Models\Config;

class UserTest extends DefaultResourceTestCase
{
	 ///////////////////////////////////////////////////////
	 //The following methods are used for providing data. //
	 //Tests follow below!								  //
	 ///////////////////////////////////////////////////////

	public function getStructure() {
		return [
			'type',
			'id',
			'attributes' => [
				'name',
				'email'
			]
		];
	}

	public static function createAdmin() {
		$userCount = (User::get()->count())+1;
		$user = User::create([
			'name' => "user{$userCount}",
			'password' => 'testtest'
		]);
		$user->attach(
			Role::findOrFail(
				Config::where(
					'key', 'serverAdminRoleId'
				)->firstOrFail()->value
			)
		);

		return $user;
	}

	public static function createUser() {
		$userCount = (User::get()->count())+1;
		$user = User::create([
			'name' => "user{$userCount}",
			'password' => 'testtest'
		]);

		$user->attach(
			Role::findOrFail(
				Config::where(
					'key', 'serverUserRoleId'
				)->firstOrFail()->value
			)
		);
		return $user;
	}

	public static function createUsers($howMany) {
		$users = [];
		for($i = 0; $i < $howMany; $i++) {
			$users[] = static::createUser();
		}
		return $users;
	}

	////////////////////////////////////////
	//Tests								  //
	////////////////////////////////////////

	public function testEveryoneCanListUsers() {
		$user = UserTest::createUsers(2);

		$this->json('GET', '/users');
		$this->assertResponseStatus(200);
        $this->seeJsonStructure(static::getCollectionStructure());

	}

	public function testEveryoneCanShowUser() {
		$user = UserTest::createUser();

		$this->json("GET", "/users/{$user->id}");
		$this->assertResponseStatus(200);
		$this->seeJsonStructure($this->getResourceStrucure());
	}

	public function testEveryoneCanNotCreateUser() {
		//create first user, should have admin role
		//no auth needed
		$this->json(
			'POST',
			'/users',
			[
				'data' => [
					'type' => "user",
					'attributes' => [
						'name' => "firstuser",
						'password' => "testtest",
						'email@example.com'
					]
				]
			]
		);
		$this->assertResponseStatus(403);
	}

	public function testAdminCanCreateUser() {
		$admin = UserTest::createAdmin();

		$authToken = \JWTAuth::fromUser($admin);
		$headers = ['Authorization' => "Bearer {$authToken}"];
		$this->json(
			'POST',
			'/users',
			[
				'data' => [
					'type' => "user",
					'attributes' => [
						'name' => "firstuser",
						'password' => "testtest",
						'email@example.com'
					]
				]
			],
			$headers
		);
		$this->assertResponseStatus(201);
		$this->seeJsonStructure($this->getResourceStrucure());
	}

	public function testAdminCanUpdateOtherUser() {
		$admin = UserTest::createAdmin();
		$user = UserTest::createUser();

		$authToken = \JWTAuth::fromUser($admin);
		$headers = ['Authorization' => "Bearer {$authToken}"];
		$this->json(
			"PATCH",
			"/users/{$user->id}",
			[
				'data' => [
					'type' => "user",
					'id' => $user->id,
					'attributes' => [
						'name' => 'yeaaaah',
						'password' => "blablabla"
					]
				]
			],
			$headers
		);

//		$this->dump();
        $this->assertResponseStatus(200);
		$this->seeJsonStructure($this->getResourceStrucure());
	}

	public function testUserCanNotCreateUser() {
		$user = UserTest::createUser();

		//create first user, should have admin role
		$authToken = \JWTAuth::fromUser($user);
		$headers = ['Authorization' => "Bearer {$authToken}"];
		$this->json(
			'POST',
			'/users',
			[
				'data' => [
					'type' => "user",
					'attributes' => [
						'name' => "firstuser",
						'password' => "testtest",
						'email@example.com'
					]
				]
			],
			$headers
		);
		$this->assertResponseStatus(403);
	}

	public function testUserCannotUpdateOtherUser() {
		$users = UserTest::createUsers(2);

		$authToken = \JWTAuth::fromUser($users[0]);
		$headers = ['Authorization' => "Bearer {$authToken}"];
		$this->json(
			"PATCH",
			"/users/{$users[1]->id}",
			[
				'data' => [
					'type' => "user",
					'id' => $users[1]->id,
					'attributes' => [
						'name' => 'yeaaaah',
						'password' => "blablabla"
					]
				]
			],
			$headers
		);
        $this->assertResponseStatus(403);
	}

	public function testUserCanUpdateHimself() {
		$user = UserTest::createUser();

		$authToken = \JWTAuth::fromUser($user);
		$headers = ['Authorization' => "Bearer {$authToken}"];
		$this->json(
			"PATCH",
			"/users/{$user->id}",
			[
				'data' => [
					'type' => "user",
					'id' => $user->id,
					'attributes' => [
						'name' => 'yeaaaah',
						'password' => "blablabla"
					]
				]
			],
			$headers
		);
        $this->assertResponseStatus(200);
		$this->seeJsonStructure($this->getResourceStrucure());
	}

	public function testAdminCanDeleteOtherUser() {
		$admin = UserTest::createAdmin();
		$user = UserTest::createUser();

		$authToken = \JWTAuth::fromUser($admin);
		$headers = ['Authorization' => "Bearer {$authToken}"];
		$this->json("DELETE", "/users/{$user->id}", [], $headers);
        $this->assertResponseStatus(204);
	}

	public function testUserCannotDeleteOtherUser() {
		$users = UserTest::createUsers(2);

		$authToken = \JWTAuth::fromUser($users[0]);
		$headers = ['Authorization' => "Bearer {$authToken}"];
		$this->json("DELETE", "/users/{$users[1]->id}", [], $headers);
        $this->assertResponseStatus(403);
	}

	public function testUserCanDeleteHimself() {
		$user = UserTest::createUser();

		$authToken = \JWTAuth::fromUser($user);
		$headers = ['Authorization' => "Bearer {$authToken}"];
		$this->json("DELETE", "/users/{$user->id}", [], $headers);
        $this->assertResponseStatus(204);
	}
}
