<?php

namespace Netmon\Server\Tests;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\TestCase;

use Netmon\Server\App\Models\User;
use Netmon\Server\App\Models\Role;
use Netmon\Server\App\Models\Permission;

abstract class ModuleTestCase extends ResourceTestCase
{
    //rollback and migrate database before each test
    use DatabaseMigrations;

    /**
     * The base URL to use while testing the application.
     *
     * @var string
     */
    protected $baseUrl = 'http://localhost';

    /**
	 * Do this before each test
     */
	public function setUp() {
		parent::setUp();
    }

    /**
     * Do this after each test
     */
    public function tearDown() {
        parent::tearDown();
    }

    /**
     * Creates the application.
     *
     * @return \Illuminate\Foundation\Application
     */
    public function createApplication()
    {
        $app = require __DIR__.'/../bootstrap/app.php';

        foreach($this->moduleServiceProviderDepencies() as $serviceProvider) {
            $app->register($serviceProvider);
        }
        $app->register($this->moduleServiceProvider());

        $app->make(\Illuminate\Contracts\Console\Kernel::class)->bootstrap();

        return $app;
    }

    // http://stackoverflow.com/a/27913150/6517870
    protected abstract function moduleServiceProvider();
}
