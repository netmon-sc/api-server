<?php

namespace Netmon\Server\Tests;

use JWTAuth;

class JwtTestCase extends DefaultResourceTestCase
{
	public function getStructure() {
        return [
            'type',
            'attributes' => [
                'token'
            ]
        ];
    }

	public function testRequestNewJwt() {
        $user = UserTest::createUser();
        $this->json(
			'POST',
			'/jwt',
			[
				'data' => [
					'type' => "jwt-request",
					'attributes' => [
						'username' => $user->name,
						'password' => "testtest"
					]
				]
			]
		);
		$this->assertResponseStatus(201);
		$this->seeJsonStructure($this->getResourceStrucure());
	}
}
