<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Symfony\Component\Console\Output\ConsoleOutput;
use Illuminate\Contracts\Validation\ValidationException;
use Netmon\Server\App\Models\User;
use Netmon\Server\App\Models\Role;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        DB::transaction(function () {
            //config
            $userNumber = 50;

        	$output = new ConsoleOutput();

            //fetch roles from database
            $adminRole = Role::findOrFail(config('netmon.roles.admin'));
            $defaultRoles = collect();
            foreach(config('netmon.roles.default') as $role) {
                $defaultRoles = $defaultRoles->push(Role::findOrFail($role));
            }
            $output->writeln("Roles fetched.");

            //create fake users
	        try {
		        $users = factory(User::class, $userNumber)->create();
            } catch(ValidationException $e) {
	       		print_r($e->errors());
            }
            $output->writeln("Created {$userNumber} fake users.");

            //create user and push it into the users collection
            $users = $users->push(User::create([
                'name' => "user",
                'password' => "12345678"
            ]));

            //create admin and push it onto the users collection
            $admin = User::create([
                'name' => "admin",
                'password' => "12345678"
            ]);
            $admin->attach($adminRole);
            $users = $users->push($admin);
            $output->writeln("Created users 'admin: 12345678' and 'user: 12345678'.");

            //assign default roles to all users
            foreach($users as $user) {
                foreach($defaultRoles as $defaultRole) {
                    $user->attach($defaultRole);
                }
            }
            $output->writeln("Roles attached to users.");
        });
        Model::reguard();
    }
}
