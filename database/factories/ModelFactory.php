<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create resources for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(Netmon\Server\App\Models\User::class, function (Faker\Generator $faker) {
	$name = $faker->unique->userName;
    return [
        'name' => $name,
        'email' => $faker->email,
        'password' => 'password'.$name,
        'remember_token' => str_random(10),
    ];
});

$factory->define(Netmon\Server\App\Models\Device::class, function ($faker) {
	return [
			'general_hostname' => $faker->domainName,
			'general_description' => $faker->optional($weight = 0.2)->sentence($nbWords = 6),
			
			//hardware
			'hardware_manufacturer' => $faker->optional($weight = 0.9)->company,
			'hardware_model' => $faker->optional($weight = 0.9)->randomElement($array = array ('WR841N','TL-WR1043N/ND2','WDR3600 | CPE')),
			'hardware_version' => $faker->optional($weight = 0.9)->randomElement($array = array ('v1','v2','v3')),
			'hardware_cpu' => $faker->optional($weight = 0.9)->randomElement($array = array ('MIPS 74Kc V4.12','BLUBB 87Kc V4.13','KAPP 74Kc V4.23')),
			
			//operating system
			'operating_system_name' => $faker->optional($weight = 0.9)->randomElement($array = array ('OpenWRT', 'DDWRT', 'FiPUX')),
			'operating_system_kernel' => $faker->optional($weight = 0.9)->randomElement($array = array ('3.16.8', '3.18', '4.2', '3.4')),
			'operating_system_version' => $faker->optional($weight = 0.5)->randomElement($array = array ('0.5.2', '0.6', '0.6.1')),
			'operating_system_revision' => $faker->optional($weight = 0.2)->randomElement($array = array ('f30d257045907cd65784643bcf5d81da2e997ef5', 'ed9adebe55c82145a5bfad4a998fa58fa4e8b000', 'd842b17eeb9c870131b802a86f13de00d3176ed4', '23007', '569836')),
			'operating_system_description' => $faker->optional($weight = 0.1)->sentence($nbWords = 6),
			
			//resources
			'resources_memory_total' => $faker->optional($weight = 0.9)->randomNumber(),
			'resources_swap_total' => $faker->optional($weight = 0.2)->randomNumber(),
			'resources_cpu_frequency' => $faker->optional($weight = 0.9)->randomNumber(),
			'resources_flash_total' => $faker->optional($weight = 0.9)->randomNumber(),
			'resources_storage_total' => $faker->optional($weight = 0.3)->randomNumber()
	];
});

$factory->define(Netmon\Server\App\Models\DeviceState::class, function ($faker) {
	return [
			'general_local_time' => $faker->optional($weight = 0.9)->unixTime(),
			'general_uptime' => $faker->optional($weight = 0.9)->randomNumber(),
			'resources_load_1min' => $faker->optional($weight = 0.9)->randomFloat($nbMaxDecimals = NULL, $min = 0, $max = 1),
			'resources_load_5min' => $faker->optional($weight = 0.9)->randomFloat($nbMaxDecimals = NULL, $min = 0, $max = 1),
			'resources_load_15min' => $faker->optional($weight = 0.9)->randomFloat($nbMaxDecimals = NULL, $min = 0, $max = 1),
			'resources_memory_total' => $faker->optional($weight = 0.9)->randomNumber(),
			'resources_memory_free' => $faker->optional($weight = 0.9)->randomNumber(),
			'resources_memory_buffered' => $faker->optional($weight = 0.9)->randomNumber(),
			'resources_memory_cache' => $faker->optional($weight = 0.9)->randomNumber(),
			'resources_swap_total' => $faker->optional($weight = 0.9)->randomNumber(),
			'resources_swap_free' => $faker->optional($weight = 0.9)->randomNumber(),
			'resources_connections_ipv4_tcp' => $faker->optional($weight = 0.9)->randomNumber(),
			'resources_connections_ipv4_udp' => $faker->optional($weight = 0.9)->randomNumber(),
			'resources_connections_ipv6_tcp' => $faker->optional($weight = 0.9)->randomNumber(),
			'resources_connections_ipv6_udp' => $faker->optional($weight = 0.9)->randomNumber(),
			'resources_processes_running' => $faker->optional($weight = 0.9)->randomNumber(),
			'resources_processes_sleeping' => $faker->optional($weight = 0.9)->randomNumber(),
			'resources_processes_blocked' => $faker->optional($weight = 0.9)->randomNumber(),
			'resources_processes_zombie' => $faker->optional($weight = 0.9)->randomNumber(),
			'resources_processes_stopped' => $faker->optional($weight = 0.9)->randomNumber(),
			'resources_processes_paging' => $faker->optional($weight = 0.9)->randomNumber(),
			'resources_cpu_frequency' => $faker->optional($weight = 0.9)->randomNumber(),
			'resources_cpu_user' => $faker->optional($weight = 0.9)->randomNumber(),
			'resources_cpu_system' => $faker->optional($weight = 0.9)->randomNumber(),
			'resources_cpu_nice' => $faker->optional($weight = 0.9)->randomNumber(),
			'resources_cpu_iowait' => $faker->optional($weight = 0.9)->randomNumber(),
			'resources_cpu_irq' => $faker->optional($weight = 0.9)->randomNumber(),
			'resources_cpu_softirq' => $faker->optional($weight = 0.9)->randomNumber(),
			'resources_flash_total' => $faker->optional($weight = 0.9)->randomNumber(),
			'resources_flash_free' => $faker->optional($weight = 0.9)->randomNumber(),
			'resources_storage_total' => $faker->optional($weight = 0.9)->randomNumber(),
			'resources_storage_free' => $faker->optional($weight = 0.9)->randomNumber()
	];
});