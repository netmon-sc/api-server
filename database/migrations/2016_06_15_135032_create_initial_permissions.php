<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use ApiServer\Authorization\Models\Role;
use ApiServer\Authorization\Models\Permission;
use ApiServer\Configs\Models\Config;

class CreateInitialPermissions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $adminRoleId = Config::where(
            'key',
            'serverAdminRoleId'
        )->firstOrFail()->value;

        //Permissions for Permissions ++++++++++++++++++++++
        Permission::create([
            'role_id' => $adminRoleId,
            'action_id' => 'index',
            'resource_id' => 'permission',
        ]);

        Permission::create([
            'role_id' => $adminRoleId,
            'action_id' => 'show',
            'resource_id' => 'permission',
        ]);

        Permission::create([
            'role_id' => $adminRoleId,
            'action_id' => 'store',
            'resource_id' => 'permission',
        ]);

        Permission::create([
            'role_id' => $adminRoleId,
            'action_id' => 'update',
            'resource_id' => 'permission',
        ]);

        Permission::create([
            'role_id' => $adminRoleId,
            'action_id' => 'destroy',
            'resource_id' => 'permission',
        ]);

        //Permissions for Roles ++++++++++++++++++++++
        Permission::create([
            'role_id' => $adminRoleId,
            'action_id' => 'index',
            'resource_id' => 'role',
        ]);

        Permission::create([
            'role_id' => $adminRoleId,
            'action_id' => 'show',
            'resource_id' => 'role',
        ]);

        Permission::create([
            'role_id' => $adminRoleId,
            'action_id' => 'store',
            'resource_id' => 'role',
        ]);

        Permission::create([
            'role_id' => $adminRoleId,
            'action_id' => 'update',
            'resource_id' => 'role',
        ]);

        Permission::create([
            'role_id' => $adminRoleId,
            'action_id' => 'destroy',
            'resource_id' => 'role',
        ]);

        //Permissions for Users ++++++++++++++++++++++
        Permission::create([
            'role_id' => $adminRoleId,
            'action_id' => 'index',
            'resource_id' => 'user',
        ]);

        Permission::create([
            'role_id' => $adminRoleId,
            'action_id' => 'show',
            'resource_id' => 'user',
        ]);

        Permission::create([
            'role_id' => $adminRoleId,
            'action_id' => 'store',
            'resource_id' => 'user',
        ]);

        Permission::create([
            'role_id' => $adminRoleId,
            'action_id' => 'update',
            'resource_id' => 'user',
        ]);

        Permission::create([
            'role_id' => $adminRoleId,
            'action_id' => 'destroy',
            'resource_id' => 'user',
        ]);

        // Configs
        Permission::create([
            'role_id' => $adminRoleId,
            'action_id' => 'index',
            'resource_id' => 'config',
        ]);

        Permission::create([
            'role_id' => $adminRoleId,
            'action_id' => 'show',
            'resource_id' => 'config',
        ]);

        Permission::create([
            'role_id' => $adminRoleId,
            'action_id' => 'store',
            'resource_id' => 'config',
        ]);

        Permission::create([
            'role_id' => $adminRoleId,
            'action_id' => 'update',
            'resource_id' => 'config',
        ]);

        Permission::create([
            'role_id' => $adminRoleId,
            'action_id' => 'destroy',
            'resource_id' => 'config',
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Permission::where('resource_id', '=', 'user')->delete();
        Permission::where('resource_id', '=', 'role')->delete();
        Permission::where('resource_id', '=', 'permission')->delete();
        Permission::where('resource_id', '=', 'config')->delete();
    }
}
