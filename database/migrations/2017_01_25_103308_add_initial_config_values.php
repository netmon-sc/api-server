<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

use ApiServer\Configs\Models\Config;

class AddInitialConfigValues extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Config::create([
            'key' => "serverPasswordResetURL",
            'value' => "https://url-to-web-client/set-new-password/"
        ]);

        Config::create([
            'key' => "serverPasswordResetTokenTTL",
            'value' => "1800"
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Config::where('key', '=', 'serverPasswordResetURL')->delete();
        Config::where('key', '=', 'serverPasswordResetTokenTTL')->delete();
    }
}
