# Netmon server
This is the API server of the Netmon Software Compilation (Netmon-SC). The API server contains the database and provides a REST API that clients can interact with.

## Manual
Developers and administrators we prepared a [manual](doc/index.md) for you!

# Client applications
There are various clients that use the api server. Not every application is developed in our Gitlab space, so here we maintain a list of where to fetch compatible applications from:
 * [Web-Client](https://git.nordwest.freifunk.net/netmon-sc/web-client)
 * [Node-Client](https://git.nordwest.freifunk.net/netmon-sc/node-client)
 * ...

# Contributing
## Submitting patches
Patches can be submitted using the Merge-Request link of our gitlab.

## Mailinglist
https://lists.ffnw.de/mailman/listinfo/netmon-dev

# License
See [License](LICENSE.txt)

# Bachelor thesis
The first stable version of the core application is part of a bachelor thesis. Therefore the development is closed for the public at the moment. After the release of the first stable version, this hint will be removed and the development will be opened to everyone.

## Title
Web-API zur Administration freier Funknetze

## Abstract
Freie Funknetze bezeichnen überwiegend WLAN gestützte Computernetzwerke, die sich durch besonders hohe Barrierefreiheit, Dezentralität und offene Spezifikationen auszeichnen. Sie werden in der Regel von Privatpersonen oder Vereinen mit Zielen wie der Überwindung der digitalen Spaltung oder der Herstellung möglichst starker Resistenz vor staatlichen Eingriffen aufgebaut. Der Aufbau und Betrieb dieser Netzwerke erfordert geeignete Anwendungen, die die Teilnehmer bei der Planung, Verwaltung und regelmäßigen Wartung unterstützen. Die Bachelorarbeit befasst sich mit dem vollständigen Neuentwurf einer bereits bestehenden Anwendung zur Abdeckung der genannten Bedürfnisse. Hierzu wird ein stark modularisiertes Gesamtkonzept auf Basis von Client-Server Anwendungen entworfen und anschließend die Serverseite in Form einer Web-API mittels aktueller Tools und Vorgehensweisen umgesetzt.
