# Scalability
The API server is designed to support massive horizontal scaling. This is beeing made easily by making the API stateless (no session needed, no complicated config needed when scaling). Horizontal scaling allows the userbase to grow virtually unlimited.

References:
 * https://www.digitalocean.com/company/blog/horizontally-scaling-php-applications/

## What is needed?
 * SQL-Cluster
 * HTTP Loadbalancer
 * HTTP-Servers to run the application

## When is this usefull?
In the past we experienced situations with heavy load on applications that provide necesary data to visualize the network. A very common example where newspapers or other webpages embedding our hotspot map into their articles. This gained very heavy load (untill failure) on our servers because the map did not support caching. Beside the fact that a map used for embedding in other webpages should support caching we can now prepare for heavy load directly in our API server (if map programmers are lazy) by using horizontal scaling and thus preparing for heavy load and hundrets of thousands of users if it's needed.
