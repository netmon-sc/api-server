# REST API dndpoint
Version 1.1.5 including the Devices, Places, Interfaces and Networking modules.

```
[floh1111@flohlap api-server]$ php artisan route:list
+--------+-----------+----------------------------------------------+-------------------------------+---------------------------------------------------------------------------+---------------+
| Domain | Method    | URI                                          | Name                          | Action                                                                    | Middleware    |
+--------+-----------+----------------------------------------------+-------------------------------+---------------------------------------------------------------------------+---------------+
|        | GET|HEAD  | /                                            |                               | Netmon\Server\App\Http\Controllers\RootController@index                   | cors,auth.jwt |
|        | POST      | auth-token/create                            |                               | Netmon\Server\App\Http\Controllers\AuthTokenController@create             | cors          |
|        | POST      | auth-token/refresh                           |                               | Netmon\Server\App\Http\Controllers\AuthTokenController@refresh            | cors          |
|        | GET|HEAD  | configs                                      | configs.index                 | Netmon\Server\App\Http\Controllers\ConfigsController@index                | cors,auth.jwt |
|        | POST      | configs                                      | configs.store                 | Netmon\Server\App\Http\Controllers\ConfigsController@store                | cors,auth.jwt |
|        | PUT|PATCH | configs/{config}                             | configs.update                | Netmon\Server\App\Http\Controllers\ConfigsController@update               | cors,auth.jwt |
|        | DELETE    | configs/{config}                             | configs.destroy               | Netmon\Server\App\Http\Controllers\ConfigsController@destroy              | cors,auth.jwt |
|        | GET|HEAD  | configs/{config}                             | configs.show                  | Netmon\Server\App\Http\Controllers\ConfigsController@show                 | cors,auth.jwt |
|        | GET|HEAD  | coordinates                                  | coordinates.index             | \Netmon\Places\Http\Controllers\CoordinatesController@index               | cors,auth.jwt |
|        | POST      | coordinates                                  | coordinates.store             | \Netmon\Places\Http\Controllers\CoordinatesController@store               | cors,auth.jwt |
|        | DELETE    | coordinates/{coordinate}                     | coordinates.destroy           | \Netmon\Places\Http\Controllers\CoordinatesController@destroy             | cors,auth.jwt |
|        | GET|HEAD  | coordinates/{coordinate}                     | coordinates.show              | \Netmon\Places\Http\Controllers\CoordinatesController@show                | cors,auth.jwt |
|        | PUT|PATCH | coordinates/{coordinate}                     | coordinates.update            | \Netmon\Places\Http\Controllers\CoordinatesController@update              | cors,auth.jwt |
|        | POST      | device-authorizations                        | device-authorizations.store   | \Netmon\Devices\Http\Controllers\DeviceAuthorizationsController@store     | cors,auth.jwt |
|        | GET|HEAD  | device-authorizations                        | device-authorizations.index   | \Netmon\Devices\Http\Controllers\DeviceAuthorizationsController@index     | cors,auth.jwt |
|        | DELETE    | device-authorizations/{device_authorization} | device-authorizations.destroy | \Netmon\Devices\Http\Controllers\DeviceAuthorizationsController@destroy   | cors,auth.jwt |
|        | PUT|PATCH | device-authorizations/{device_authorization} | device-authorizations.update  | \Netmon\Devices\Http\Controllers\DeviceAuthorizationsController@update    | cors,auth.jwt |
|        | GET|HEAD  | device-authorizations/{device_authorization} | device-authorizations.show    | \Netmon\Devices\Http\Controllers\DeviceAuthorizationsController@show      | cors,auth.jwt |
|        | POST      | device-statuses                              | device-statuses.store         | \Netmon\Devices\Http\Controllers\DeviceStatusesController@store           | cors,auth.jwt |
|        | GET|HEAD  | device-statuses                              | device-statuses.index         | \Netmon\Devices\Http\Controllers\DeviceStatusesController@index           | cors,auth.jwt |
|        | DELETE    | device-statuses/{device_status}              | device-statuses.destroy       | \Netmon\Devices\Http\Controllers\DeviceStatusesController@destroy         | cors,auth.jwt |
|        | PUT|PATCH | device-statuses/{device_status}              | device-statuses.update        | \Netmon\Devices\Http\Controllers\DeviceStatusesController@update          | cors,auth.jwt |
|        | GET|HEAD  | device-statuses/{device_status}              | device-statuses.show          | \Netmon\Devices\Http\Controllers\DeviceStatusesController@show            | cors,auth.jwt |
|        | POST      | devices                                      | devices.store                 | \Netmon\Devices\Http\Controllers\DevicesController@store                  | cors,auth.jwt |
|        | GET|HEAD  | devices                                      | devices.index                 | \Netmon\Devices\Http\Controllers\DevicesController@index                  | cors,auth.jwt |
|        | POST      | devices/{deviceId}/become-owner              |                               | \Netmon\Devices\Http\Controllers\DevicesController@becomeOwner            | cors,auth.jwt |
|        | PATCH     | devices/{deviceId}/status                    |                               | \Netmon\Devices\Http\Controllers\DeviceNestsDeviceStatusController@update | cors,auth.jwt |
|        | GET|HEAD  | devices/{deviceId}/status                    |                               | \Netmon\Devices\Http\Controllers\DeviceNestsDeviceStatusController@show   | cors,auth.jwt |
|        | DELETE    | devices/{deviceId}/status                    |                               | \Netmon\Devices\Http\Controllers\DeviceNestsDeviceStatusController@delete | cors,auth.jwt |
|        | DELETE    | devices/{device}                             | devices.destroy               | \Netmon\Devices\Http\Controllers\DevicesController@destroy                | cors,auth.jwt |
|        | PUT|PATCH | devices/{device}                             | devices.update                | \Netmon\Devices\Http\Controllers\DevicesController@update                 | cors,auth.jwt |
|        | GET|HEAD  | devices/{device}                             | devices.show                  | \Netmon\Devices\Http\Controllers\DevicesController@show                   | cors,auth.jwt |
|        | POST      | ips                                          | ips.store                     | \Netmon\Networking\Http\Controllers\IpsController@store                   | cors,auth.jwt |
|        | GET|HEAD  | ips                                          | ips.index                     | \Netmon\Networking\Http\Controllers\IpsController@index                   | cors,auth.jwt |
|        | GET|HEAD  | ips/{ip}                                     | ips.show                      | \Netmon\Networking\Http\Controllers\IpsController@show                    | cors,auth.jwt |
|        | DELETE    | ips/{ip}                                     | ips.destroy                   | \Netmon\Networking\Http\Controllers\IpsController@destroy                 | cors,auth.jwt |
|        | PUT|PATCH | ips/{ip}                                     | ips.update                    | \Netmon\Networking\Http\Controllers\IpsController@update                  | cors,auth.jwt |
|        | GET|HEAD  | me                                           |                               | Netmon\Server\App\Http\Controllers\MeController@show                      | cors,auth.jwt |
|        | POST      | modules                                      | modules.store                 | Netmon\Server\App\Http\Controllers\ModulesController@store                | cors,auth.jwt |
|        | GET|HEAD  | modules                                      | modules.index                 | Netmon\Server\App\Http\Controllers\ModulesController@index                | cors,auth.jwt |
|        | PUT|PATCH | modules/{module}                             | modules.update                | Netmon\Server\App\Http\Controllers\ModulesController@update               | cors,auth.jwt |
|        | DELETE    | modules/{module}                             | modules.destroy               | Netmon\Server\App\Http\Controllers\ModulesController@destroy              | cors,auth.jwt |
|        | GET|HEAD  | modules/{module}                             | modules.show                  | Netmon\Server\App\Http\Controllers\ModulesController@show                 | cors,auth.jwt |
|        | POST      | network-interfaces                           | network-interfaces.store      | \Netmon\Interfaces\Http\Controllers\NetworkInterfacesController@store     | cors,auth.jwt |
|        | GET|HEAD  | network-interfaces                           | network-interfaces.index      | \Netmon\Interfaces\Http\Controllers\NetworkInterfacesController@index     | cors,auth.jwt |
|        | DELETE    | network-interfaces/{network_interface}       | network-interfaces.destroy    | \Netmon\Interfaces\Http\Controllers\NetworkInterfacesController@destroy   | cors,auth.jwt |
|        | GET|HEAD  | network-interfaces/{network_interface}       | network-interfaces.show       | \Netmon\Interfaces\Http\Controllers\NetworkInterfacesController@show      | cors,auth.jwt |
|        | PUT|PATCH | network-interfaces/{network_interface}       | network-interfaces.update     | \Netmon\Interfaces\Http\Controllers\NetworkInterfacesController@update    | cors,auth.jwt |
|        | GET|HEAD  | permissions                                  | permissions.index             | Netmon\Server\App\Http\Controllers\PermissionsController@index            | cors,auth.jwt |
|        | POST      | permissions                                  | permissions.store             | Netmon\Server\App\Http\Controllers\PermissionsController@store            | cors,auth.jwt |
|        | GET|HEAD  | permissions/{permission}                     | permissions.show              | Netmon\Server\App\Http\Controllers\PermissionsController@show             | cors,auth.jwt |
|        | DELETE    | permissions/{permission}                     | permissions.destroy           | Netmon\Server\App\Http\Controllers\PermissionsController@destroy          | cors,auth.jwt |
|        | PUT|PATCH | permissions/{permission}                     | permissions.update            | Netmon\Server\App\Http\Controllers\PermissionsController@update           | cors,auth.jwt |
|        | GET|HEAD  | resources                                    |                               | Netmon\Server\App\Http\Controllers\ResourcesController@index              | cors,auth.jwt |
|        | POST      | roles                                        | roles.store                   | Netmon\Server\App\Http\Controllers\RolesController@store                  | cors,auth.jwt |
|        | GET|HEAD  | roles                                        | roles.index                   | Netmon\Server\App\Http\Controllers\RolesController@index                  | cors,auth.jwt |
|        | GET|HEAD  | roles/{role}                                 | roles.show                    | Netmon\Server\App\Http\Controllers\RolesController@show                   | cors,auth.jwt |
|        | DELETE    | roles/{role}                                 | roles.destroy                 | Netmon\Server\App\Http\Controllers\RolesController@destroy                | cors,auth.jwt |
|        | PUT|PATCH | roles/{role}                                 | roles.update                  | Netmon\Server\App\Http\Controllers\RolesController@update                 | cors,auth.jwt |
|        | POST      | users                                        | users.store                   | Netmon\Server\App\Http\Controllers\UsersController@store                  | cors,auth.jwt |
|        | GET|HEAD  | users                                        | users.index                   | Netmon\Server\App\Http\Controllers\UsersController@index                  | cors,auth.jwt |
|        | POST      | users/initiate-password-reset                |                               | Netmon\Server\App\Http\Controllers\UsersController@initiatePasswordReset  | cors          |
|        | POST      | users/signup                                 |                               | Netmon\Server\App\Http\Controllers\UsersController@signup                 | cors          |
|        | GET|HEAD  | users/{user}                                 | users.show                    | Netmon\Server\App\Http\Controllers\UsersController@show                   | cors,auth.jwt |
|        | PUT|PATCH | users/{user}                                 | users.update                  | Netmon\Server\App\Http\Controllers\UsersController@update                 | cors,auth.jwt |
|        | DELETE    | users/{user}                                 | users.destroy                 | Netmon\Server\App\Http\Controllers\UsersController@destroy                | cors,auth.jwt |
+--------+-----------+----------------------------------------------+-------------------------------+---------------------------------------------------------------------------+---------------+
```
