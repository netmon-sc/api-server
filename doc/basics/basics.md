# Development basics
This file contains some usefull hints for developing the Netmon Core module.

## Steps to create a new ressource
Add a new Model for your ressource to app/Models/
 * http://laravel.com/docs/5.1/eloquent#defining-models
 * php artisan make:model Models/User
 
Add a new migration for the database table used by the model to database/migrations/
 * http://laravel.com/docs/5.1/migrations
 * php artisan make:migration create_users_table --create=users
 
Add a new transformer to transform the data of the model to the API format to app/Transformers/
 * https://github.com/Cyvelnet/laravel5-fractal
 * php artisan make:transformer NetworkDeviceTransformer
 
Add a new ressource controller (note the Api/v1/ version string!) to app/Http/Controllers/Api/vX/
 * http://laravel.com/docs/5.1/controllers#restful-resource-controllers
 * php artisan make:controller Api/v1/PhotoController

Add a new ressourcefull route to the app/route.php

Add a new factory to database/factories/ModelFactory.php. Example:
```
$factory->define(App\Models\NetworkDevice::class, function ($faker) {
	return [
			'hostname' => $faker->domainName,
	];
});
```

Add new seed to database/seeds/DatabaseSeeder.php. Example:
```
factory(App\Models\NetworkDevice::class, 50)->create();
```

## Generate web API documentation
The Web-API is documented using the blueprint annotation from dingo in our api controller classes.

```
cd ./src
php artisan api:docs --name Netmon --use-version v1 --output-file ../doc/web-api/v1.md
```

References:
 * https://github.com/dingo/api/wiki/API-Blueprint-Documentation

## Check coding style
We use PHP CodeSniffer to check if our code meets the `PEAR` coding standard.

Use the following statement for code checking
```
cd src/
php ./vendor/squizlabs/php_codesniffer/scripts/phpcs app/Models/User.php
```

Use the following statements for autocorrection of styling errors:

Print a diff:
```
cd src/
phpcs --report=diff /path/to/code
```

Generate patch and patch file manually
```
phpcs --report-diff=/path/to/changes.diff /path/to/code
patch -p0 -ui /path/to/changes.diff
```

For more automatically code style fixing see https://github.com/squizlabs/PHP_CodeSniffer/wiki/Fixing-Errors-Automatically

References:
 * https://github.com/squizlabs/PHP_CodeSniffer
 * https://pear.php.net/manual/en/standards.php

## Seggestions to take a look at
```
[floh1111@flohlap src]$ composer require dingo/api:1.0.x@dev
./composer.json has been updated
> php artisan clear-compiled
Loading composer repositories with package information
Updating dependencies (including require-dev)
  - Installing doctrine/lexer (v1.0.1)
    Loading from cache

  - Installing doctrine/annotations (v1.2.7)
    Downloading: 100%         

  - Installing dingo/blueprint (v0.1.3)
    Downloading: 100%         

  - Installing league/fractal (0.12.0)
    Loading from cache

  - Installing dingo/api (dev-master 4a378a8)
    Cloning 4a378a809f63cfe8dc3303fa739f9eb76f3b18d4

league/fractal suggests installing pagerfanta/pagerfanta (Pagerfanta Paginator)
league/fractal suggests installing zendframework/zend-paginator (Zend Framework Paginator)
dingo/api suggests installing tymon/jwt-auth (Protect your API with JSON Web Tokens)
dingo/api suggests installing lucadegasperi/oauth2-server-laravel (Protect your API with OAuth 2.0.)
Writing lock file
Generating autoload files
> php artisan optimize
Generating optimized class loader
[floh1111@flohlap src]$ composer require tymon/jwt-auth
Using version ^0.5.5 for tymon/jwt-auth
./composer.json has been updated
> php artisan clear-compiled
Loading composer repositories with package information
Updating dependencies (including require-dev)
  - Installing phpseclib/phpseclib (0.3.10)
    Downloading: 100%         

  - Installing namshi/jose (5.0.2)
    Downloading: 100%         

  - Installing tymon/jwt-auth (0.5.5)
    Downloading: 100%         

phpseclib/phpseclib suggests installing ext-mcrypt (Install the Mcrypt extension in order to speed up a wide variety of cryptographic operations.)
phpseclib/phpseclib suggests installing ext-gmp (Install the GMP (GNU Multiple Precision) extension in order to speed up arbitrary precision integer arithmetic operations.)
phpseclib/phpseclib suggests installing pear-pear/PHP_Compat (Install PHP_Compat to get phpseclib working on PHP < 4.3.3.)
Writing lock file
Generating autoload files
> php artisan optimize
Generating optimized class loader
```