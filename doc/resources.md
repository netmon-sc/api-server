# Resources
This page describes how to add a new resource to the API-Server. Resources in terms of REST usually stand for an API endpoint. A GET request to `http://localhost:8000/users` would mean a GET request to the `users` resource.

*Please note: usually you want to add new resources using a [module](doc/modules/modules.md). For information on how to add a new resource to a module please view the module manual.*

## Creating a new resource
A new resource for the api server can be created by doing the following steps:
1. create a new database table for your resource using migrations in `database/migrations/` ([see Laravel manual](https://laravel.com/docs/5.3/migrations))
1. create a new model in `app/Models/` ([see Laravel manual](https://laravel.com/docs/5.3/eloquent))
1. create a policy in app/Policies ([see Laravel manual](https://laravel.com/docs/5.3/authorisation))
1. link the created policy to a model at `app/Prividers/AuthServiceProvider.php`
1. create a new serializer at `app/Serializers/`
1. create a new controller at `app/Http/Controllers/`
1. create an entry for your resource in `app/Http/routes.php`
