Some resources need authentication. This page describes how the api server handles authentication. The examples on this page can be executed after resetting and seeding the database with testdata like described in the [database manual](doc/database/database.md#generate-testdata).

<!-- TOC depthFrom:1 depthTo:6 withLinks:1 updateOnSave:1 orderedList:0 -->

- [Authentication](#authentication)
	- [Request a new JWT](#request-a-new-jwt)
	- [Accessing an authenticated route](#accessing-an-authenticated-route)

<!-- /TOC -->

# Authentication
Authentication is done using [JSON Web Tokens](https://jwt.io/) (JWT) holding the id of the authenticated user as payload. This is done to get rid of classic stefull sessions and achieve the goal of a REST API to be  stateless (also on the authentication layer).

The authentication process is as follows:

Approach using long living auth-token
1. Request a new authentication token with a long lifetime (1 month) for your user by POSTing `username` and `password` to the `auth-token/` resource
1. Store the received authentication token in local storage. This makes it accessible by your javascript client application but also vulnerable to XSS attacks. So make shure to use a Javascript framework that has build-in security mechanisms agains XSS vulnerability (in example Angular 2, see https://angular.io/docs/ts/latest/guide/security.html)
1. Send a request to the resource you want to access including the authentication token in the `Authorisation`-Header
1. If the authentication token is near to expire, then refresh the authentication token by making an authenticated GET request to the `auth-token-refresh` resource

Approach using short living auth-token and long living refresh token (???)
 1. Request a new authentication token (short lifetime, about 30 minutes) for your user by POSTing `username` and `password` to the `auth-token/` resource
 1. Store the received authentication token in local storage. This makes it accessible by your javascript client application but also vulnerable to XSS attacks
 1. Request a new refresh-token (long lifetime, about 1 month) for your authentication token by accessing the `refresh-token/` resource using an authenticated request
 1. The api server will respond to you request by sending you a cookie containing the refresh token. This cookie has the `httpOnly` flag enabled. Thus it will not be accessible by your javascript client application but only by the api server itself. Therefore it is secured against XSS attacks but vulnerable to XSRF attacks instead.



## Request a new JWT
To request a new JWT for your user you need to send a `jwt-request` document to the `jwt` resource.

Example:
```
curl -H "Content-Type: application/json" -X POST -d '{"data": {"type": "jwt-request", "attributes": {"username": "user", "password": "12345678"}}}' http://localhost:8000/auth-token
```

Response:
```
{
  "data": {
    "type": "jwt",
    "attributes": {
      "token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjUxLCJpc3MiOiJodHRwczpcL1wvYXBpLm5ldG1vbi5mZm53LmRlXC9qd3QiLCJpYXQiOjE0Njk0NjY1NjMsImV4cCI6MTQ2OTQ3MDE2MywibmJmIjoxNDY5NDY2NTYzLCJqdGkiOiIzNWQ4MjlkY2M4YTM1ZmRlOWE1MTMxZWE2OTFhNjQ1MiJ9.v96RVii_VscfG4apUOKbzn0P6PbTWHjBxmTa_ffvmdM"
    }
  }
}
```

The string inside the `token` attribute should be stored on client side and can be used in the `Authorisation` header to call an authenticated route. The `Authorisation` Header using the above JWT should look like this: `Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjUxLCJpc3MiOiJodHRwczpcL1wvYXBpLm5ldG1vbi5mZm53LmRlXC9qd3QiLCJpYXQiOjE0Njk0NjY1NjMsImV4cCI6MTQ2OTQ3MDE2MywibmJmIjoxNDY5NDY2NTYzLCJqdGkiOiIzNWQ4MjlkY2M4YTM1ZmRlOWE1MTMxZWE2OTFhNjQ1MiJ9.v96RVii_VscfG4apUOKbzn0P6PbTWHjBxmTa_ffvmdM`

## Accessing an authenticated route
Accessing an authenticated route can be done by adding the JWT in the `Authorisation` Header.

Example of updating the password of a user:
```
curl -H "Content-Type: application/json" -H "Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjUxLCJpc3MiOiJodHRwczpcL1wvYXBpLm5ldG1vbi5mZm53LmRlXC9qd3QiLCJpYXQiOjE0Njk0NjY1NjMsImV4cCI6MTQ2OTQ3MDE2MywibmJmIjoxNDY5NDY2NTYzLCJqdGkiOiIzNWQ4MjlkY2M4YTM1ZmRlOWE1MTMxZWE2OTFhNjQ1MiJ9.v96RVii_VscfG4apUOKbzn0P6PbTWHjBxmTa_ffvmdM" -X PATCH -d '{"data": {"type": "user", "attributes": {"password": "newpassword"}}}' http://localhost:8000/users/51
```
