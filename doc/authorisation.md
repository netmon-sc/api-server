# Authorisation

To control which user can access which resources the netmon-sc core module comes with an [Attribute-Based Access Control](https://en.wikipedia.org/wiki/Attribute-Based_Access_Control) system based on [Laravels gate and policy technology](https://laravel.com/docs/5.2/authorization).

Attribute Based Access Controll allows:
 * [Discretionary Access Control](https://de.wikipedia.org/wiki/Discretionary_Access_Control)
 * [Role Based Access Control](https://de.wikipedia.org/wiki/Role_Based_Access_Control)
 * [Mandatory Access Control](https://de.wikipedia.org/wiki/Mandatory_Access_Control)



Note: the real implementation currently consists of a mixture of attribute based access controll and role based access control. Unification is a TODO.

[[_TOC_]]

## Resources
 * https://github.com/GeneaLabs/laravel-governor
 * http://csrc.nist.gov/projects/abac/

## Implementation
### Details
#### Models
Models are a closed set of resources that are available in the core module. Each model is mapped to a [policy class](https://laravel.com/docs/5.2/authorization#policies).

Typical examples for models are:
 * Device
 * Network-Interface
 * IP-Address

#### Actions
Actions are a closed set of operations a user can do on a model. Actions are mapped to a models policy. Typical examples for actions are:
 * **index**: use for accessing index resources
 * **show**: used for accessing show resources
 * **store**: used for accessing store resources
 * **update**: used for accessing update resources
 * **delete**: used for accessing delete resources
 * **attach**: https://opencredo.com/designing-rest-api-fine-grained-resources-hateoas-hal/
 * **detach**: https://opencredo.com/designing-rest-api-fine-grained-resources-hateoas-hal/

#### Roles
A role groups several actions on entities under a single identifier. Roles can be assigned to users. A user that gets a role assigned inherits all actions assigned to this role. Typical examples for roles:
 * Systemadmin (can do everything, role is added during installation of core module)
 * Hoodadmin (can create new hoods, role is added during installation of hoods plugin)
 * User (can create new devices, role is added during installation of core module)

#### Permissions
A permission is the combination of an action, an entity and optional an object_id. If a permission has an object_id defined, the entity and the object_id can be used to resolve an actual object in the database.

Permissions are used to control access of an actual user to a resource so they are always linked either to a role or to an user.

### Database design
To make authorisation settings persistant there are several database tables involved:
 * **entities**:
 * **actions**:
 * **roles**: Groups several actions on entities so they can be assigned to a user at once.
 * **role_user**: Mapps roles to users.

```
 # Create DB migration
 # Create Model
 # Create Controller
 php artisan make:policy DevicePolicy
 # Register the policy in the $policies property of the AuthServiceProvider
 # Add action-methods to policy
 # test

## TODO
 * Add entities
 * Add roles
