This document contains basic information about working with the database.



# Basics
## Supported Database Systems
 * MySQL (default)
 * PostgreSQL (needs to be verified)
 * Microsoft SQL Server (needs to be verified)

## Important places
The [database/](../../database) folder contains all the important stuff concerning the database:
 * [database/migrations](../../database/migrations) create the database schema and insert basic setup (basic roles and permissions i.e.). See the [Laravel documentation on migrations](https://laravel.com/docs/5.2/migrations) for more information.
 * [database/seeds](../../database/seeds) used to seed the database with testdata. See the [Laravel documentation on seeding](https://laravel.com/docs/5.2/seeding) for more information.
 * [database/factories](../../database/factories) used to generate random data the is used by the seeds for generating testdata. See the [Laravel documentation on factories](https://laravel.com/docs/5.2/seeding#using-model-factories) for more information about factories.

# Working with the database
## Reset the database
To reset the database use:
```
php artisan migrate:refresh
```

## Generate testdata
Seeds are used to generate testdata. They include a basic setup with two users. View the log output for login data.

To reset the database and seed it with testdata use :
```
php artisan migrate:refresh --seed
```

To seed the database without resetting it use:
```
php artisan db:seed
```

# ER-Diagram
TODO
