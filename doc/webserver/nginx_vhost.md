# Nginx vHost
```
server {
        # We need to listen on 443 and 80 because our nodes do not have
        # enough disk space for ssl support
        listen 80;
        listen [::]:80;
        listen 443;
        listen [::]:443;

        server_name <YOURDOMAIN>;
        root  "/YOUR/WEBROOT/public/";

        try_files $uri $uri/ /index.php?$args;
        index index.php;

        # Requests to https://yourdomain.de need to be handled seperately. Otherwise
        # OPTIONS requests in / will always result in error 405
        # https://www.nginx.com/resources/wiki/start/topics/recipes/symfony/
        location / {
                # try to serve file directly, fallback to app.php
                try_files $uri /index.php$is_args$args;
        }

        location ~ \.php$ {
                include snippets/fastcgi-php.conf;
                fastcgi_pass unix:/var/run/php5-fpm.<YOURSOCKET>.sock;
        }
}
```
