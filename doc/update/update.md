# Update
To update the Netmon API-Server run the following commands in the root folder:
```
php artisan down
git pull --rebase
git checkout <version>
composer install
php artisan migrate
php artisan modules:update
php artisan up
```
