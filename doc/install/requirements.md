This document describes the requirements to run the Netmon API server.

<!-- TOC depthFrom:1 depthTo:6 withLinks:1 updateOnSave:1 orderedList:0 -->

- [Requirements](#requirements)
	- [Production](#production)
		- [PHP](#php)
		- [System](#system)
	- [Development](#development)
		- [PHP](#php)
		- [System](#system)
- [Next steps](#next-steps)

<!-- /TOC -->

# Requirements
If something goes wrong please consult the official [Laravel requirements](https://laravel.com/docs/5.2#server-requirements).

## Production
A production system has the following requirements.

### PHP
 * PHP >= 5.6
 * OpenSSL PHP Extension
 * PDO PHP Extension
 * Mbstring PHP Extension
 * Tokenizer PHP Extension
 * XML PHP Extension

### System
 * PHP Composer

## Development
A development system has all the requirements of the production system plus the following requirements defined by the tools used in composers require-dev section.

### PHP
 * intl PHP extension
 * sqlite PHP extension

### System
 * graphviz

# Next steps
 * [Installation](doc/install/installation.md)
