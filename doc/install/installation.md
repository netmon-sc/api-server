# Installation
This document describes how to install a production environment of the Netmon API server. If you just want to quickly set up a development environment for testing purposes please read the [installation manual for developers](doc/install/installation_development.md).

Please notice the [system requirements](./requirements.md).

## Fetching sources
We use composer to fetch the sources and install all depencies:
```
composer create-project netmon-server/server -s dev /PATH/TO/YOUR/FOLDER
```

Here we choose `no` so we can easily update using `git` later
```
Do you want to remove the existing VCS (.git, .svn..) history? [Y,n]? n
```

Then we change into the installation folder
```
cd /PATH/TO/YOUR/FOLDER
```

## Main configuration
The main configuration ist stored in the .env file.

Skip the following commands if you installed the application using `composer create-project`:
```
# Copy example config to production config
cp .env.example .env
# Generate app key
php artisan key:generate
# Generate JWT key
php artisan jwt:generate
```

## Adjust settings
Next we edit the .env file manually to adjust important configuration options (like db and email settings).

## Setup directory permissions
The webserver need write permissions on the following directories:
 * storage/
 * bootstrap/cache

## Setup database schema
To install the database schema run:
```
php artisan migrate
```

## Setup webserver
After you have installed the application you can run the application by accessing the `public/` folder with your webbrowser. Set the DocumentRoot of your webserver to this location.

You can find some example vHost configurarations in the [webserver](doc/webserver/)-folder.

## Install modules
```
php artisan module:install Places "Netmon\Places\ModuleServiceProvider"
php artisan module:configure Places

php artisan module:install Devices "Netmon\Devices\ModuleServiceProvider"
php artisan module:configure Devices

php artisan module:install Interfaces "Netmon\Interfaces\ModuleServiceProvider"
php artisan module:configure Interfaces

php artisan module:install Networking "Netmon\Networking\ModuleServiceProvider"
php artisan module:configure Networking

php artisan module:install Places "Netmon\Places\ModuleServiceProvider"
php artisan module:configure Places
```

## Create first user
You should now quickly install the API-Servers Devices module and the Netmon
Web-Client and signup as first user. The first user will get admin permissions.

# Next steps
 * [Install a Server-Module](../modules/modules.md)
 * [Install the Web-Client](https://git.ffnw.de/netmon-sc/web-client)
 * [Updating the API server](../update.md)
