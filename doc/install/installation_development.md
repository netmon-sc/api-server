This document describes how to install a temporary development environment of the Netmon API server. Please notice the [system requirements](./requirements.md).

## Fetch sourcecode
Fetch the sourcecode into your webservers root directory:
```
git clone https://git.nordwest.freifunk.net/netmon-sc/api-server.git
cd ./api-server
```

## Install the application
Change into the source folder and install the application:
```
composer install
```

## Setup database
Create sqlite database
```
touch storage/dev.sqlite
```

Run migrations to setup database schema in sqlite database
```
APP_ENV=dev php artisan migrate
```

## Serve your application
```
APP_ENV=dev php artisan serve
```

Now the API server will be available at http://localhost:8000
