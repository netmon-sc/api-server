## Generate code documentation from inline PhpDoc
The projects source code is documented with the PhpDoc format. The documentation can be generated with the following command and will be located in `doc/app-api/generated/`. Call this location with a webbrowser to view the documentation. 

```
cd ./src
php ./vendor/apigen/apigen/bin/apigen generate -s ./app/ -d ../doc/app-api/
```

References:
 * http://www.apigen.org/
 * https://github.com/ApiGen/ApiGen
 * http://www.phpdoc.org/docs/latest/index.html
 * https://stackoverflow.com/questions/29896171/phpdocumentor-on-laravel-framework
 
## Generate PhpDoc for Models from Database
We use the barryvdh/laravel-ide-helper package to autogenerate PhpDoc annotations for our models. Use the following command and confirm override of existing files:

```
php artisan ide-helper:models
```

This should be done before generating application API documentation.

References:
 * https://github.com/barryvdh/laravel-ide-helper