This documents contains some basic information about the Web-API. The examples on this page can be executed after resetting and seeding the database with testdata like described in the [database manual](doc/database/database.md#generate-testdata).

<!-- TOC depthFrom:1 depthTo:6 withLinks:1 updateOnSave:1 orderedList:0 -->

- [Web-API](#web-api)
	- [Available resources](#available-resources)
	- [Multilanguage](#multilanguage)
- [Examples](#examples)
	- [List users](#list-users)
	- [Show single user](#show-single-user)
	- [Create user](#create-user)
	- [Update user](#update-user)
	- [Delete user](#delete-user)

<!-- /TOC -->

# Web-API
The web API uses the [JSONAPI](http://jsonapi.org/) format.

## Available resources
To get a detailed list of all resources and their avaliable HTTP methods use:
```
php artisan route:list
```

## Multilanguage
The language of the API can be changed by adding an [ISO 639-1](https://en.wikipedia.org/wiki/List_of_ISO_639-1_codes) language code to the  accept-language header of a request:
```
accept-language: de
```

Take a look at the [resources/lang/](resources/lang/) folder to view all avaliable languages. If you want to add a translation use the `en` language as base language.

# Examples
## List users
```
curl https://api.netmon.ffnw.de/users
```

## Show single user
```
curl https://api.netmon.ffnw.de/users
```

## Create user
```
curl -H "Content-Type: application/json" -X POST -d '{"data": {"type": "user", "attributes": {"name": "mynickname", "password": "mypassword"}}}' https://api.netmon.ffnw.de/users
```

## Update user
Note: take a look at the [authentication documentation](doc/authentication.md) to learn how to create the `Authorization` header and authenticate.

```
curl -H "Content-Type: application/json" -H "Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjUxLCJpc3MiOiJodHRwczpcL1wvYXBpLm5ldG1vbi5mZm53LmRlXC9qd3QiLCJpYXQiOjE0Njk0NjY1NjMsImV4cCI6MTQ2OTQ3MDE2MywibmJmIjoxNDY5NDY2NTYzLCJqdGkiOiIzNWQ4MjlkY2M4YTM1ZmRlOWE1MTMxZWE2OTFhNjQ1MiJ9.v96RVii_VscfG4apUOKbzn0P6PbTWHjBxmTa_ffvmdM" -X PATCH -d '{"data": {"type": "user", "attributes": {"password": "newpassword"}}}' https://api.netmon.ffnw.de/users/51
```

## Delete user
Note: take a look at the [authentication documentation](doc/authentication.md) to learn how to create the `Authorization` header and authenticate.

```
curl -H "Content-Type: application/json" -H "Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjUxLCJpc3MiOiJodHRwczpcL1wvYXBpLm5ldG1vbi5mZm53LmRlXC9qd3QiLCJpYXQiOjE0Njk0NjY1NjMsImV4cCI6MTQ2OTQ3MDE2MywibmJmIjoxNDY5NDY2NTYzLCJqdGkiOiIzNWQ4MjlkY2M4YTM1ZmRlOWE1MTMxZWE2OTFhNjQ1MiJ9.v96RVii_VscfG4apUOKbzn0P6PbTWHjBxmTa_ffvmdM" -X DELETE https://api.netmon.ffnw.de/users/51
```
