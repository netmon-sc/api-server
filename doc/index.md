# Netmon API-Server manual for administrators and developers
## Administration
### Installation and configuration
* [Installation](doc/install/installation.md)
* [Scalability](doc/scalability.md)

### Updating
* [Updating](doc/update/update.md)

### Modules
* [Modules](doc/modules/modules.md)

## Development
### Resources
* [Resources](doc/resources.md)
* [API-Endpoints](doc/api/endpoints.md)

### Modules
* [Module development](doc/modules/development_new_module.md)

### API
* [Working with the web API](doc/web-api/web-api.md)
* [Working with the database](doc/database/database.md)
* [Authentication layer](doc/authenticaction.md)
* [Authorisation layer](doc/authorisation.md)

### Unittesting and Continuous Integration
* [Contiuous Integration](doc/ci.md)
* [Unittesting](doc/unittesting.md)

### Creating releases
* [Creating a release](doc/release.md)
