# Developing a new module
## Resources
# Resources
* https://websanova.com/blog/laravel/creating-a-new-package-in-laravel-5-part-1-package-workflow
* https://laravel.com/docs/5.3/packages
* http://laraveldaily.com/how-to-create-a-laravel-5-package-in-10-easy-steps/

## Folder structure
Create folder structure:
```
mkdir /some/where/on/our/disk/<MODULENAME>/
cd /some/where/on/our/disk/<MODULENAME>/
mkdir -p database/migrations/
mkdir -p Http/Controllers/
mkdir Models/
mkdir Policies/
mkdir Providers/
mkdir Serializers/
touch Http/routes.php
touch ModuleServiceProvider.php
touch Providers/AuthServiceProvider.php
touch composer.json
```

## Define composer package
Modules are defined as composer packages. So we open the `composer.json` file
and add some metadata about our module:
```
{
    "name": "netmon-server/<MODULENAME>",
    "description": "This package extends the netmon server.",
    "license": "MIT",
    "keywords": ["laravel", "freifunk", "netmon"],
    "autoload": {
        "psr-4": {
            "Netmon\\<MODULENAME>\\": "src/",
            "Netmon\\<MODULENAME>\\Http\\Controllers\\": "src/Http/Controllers/",
            "Netmon\\<MODULENAME>\\Models\\": "src/Models/",
            "Netmon\\<MODULENAME>\\Policies\\": "src/Policies/",
            "Netmon\\<MODULENAME>\\Providers\\": "src/Providers/",
            "Netmon\\<MODULENAME>\\Serializers\\": "src/Serializers/"
        }
    },
    "require": {
        "illuminate/support": "~5"
    }
}
```

## Creating the ModuleServiceProvider
To inject our module into the Netmon Server we must provide a canonical service
provider called `ModuleServiceProvider.php` in the root folder of our module.
This will be the *entry point* for injecting our module into the Netmon Server.

*The Netmon Server itself manages a list of installed module service providers
(in it's database) and will register them at runtime using it's own [ModulesServiceProvider](https://git.ffnw.de/netmon-sc/api-server/blob/master/app/Providers/ModulesServiceProvider.php).*

Open `ModuleServiceProvider.php` and add the following content:
```
<?php

namespace Netmon\<MODULENAME>;

use Illuminate\Support\ServiceProvider;

class ModuleServiceProvider extends ServiceProvider
{
    public function register()
    {
        //Register additional service providers this module has. If your module
        //does not have any additional service providers, just leave this empty.
        $this->app->register(\Netmon\<MODULENAME>\Providers\AuthServiceProvider::class);
    }

    public function boot()
    {
        require __DIR__ . '/Http/routes.php';

        //register database migrations
        //use php artisan vendor:publish --tag=migrations to publish migrations
        $this->publishes([
            __DIR__ . '/database/migrations' => $this->app->databasePath() . '/migrations'
        ], 'migrations');
    }
}
?>
```

We will use the `register()` to register any
additional service providers our module has. In this example we will have
another `Providers/AuthServiceProvider` that needs to be registered. If you
dont have any additional service providers your can leave the `register()`
method empty.

The `boot()` method is used to provide information about the routes our module
provides and about database migrations needed by our module to store data into
the database.

## Creating and additional service provider
Next we will create the mentioned `Providers/AuthServiceProvider`. This service
provider will be used to map authorisation Policies (who can see what) to
models (fetches data from database). Both of which we will create later on.
*Notice: If your module will not have any policies, you dont need to have an
AuthServiceProvider.*

Open `Providers/AuthServiceProvider.php` and add the following content:
```
<?php

namespace Netmon\<MODULENAME>\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // Comment this in when you have created a model and the
        // coresponding policy
        // \Netmon\<MODULENAME>\Models\<MODEL>::class => \Netmon\<MODULENAME>\Policies\<POLICY>::class
    ];

    /**
     * Register any application authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();
    }
}
```

Currently we leave the `$policies` array empty. But we can fill it up when
we have created models and their coresponding policies.

## Creating routes
Last but not least we will create the file that will define the available URLs
(routes). Open `Http/routes.php` and add the following content:
```
<?php

Route::group([
    'namespace' => '\Netmon\<MODULENAME>\Http\Controllers',
    'middleware' => 'cors'], function()
{
	/**
	 * Routes using JWT auth
	 */
	Route::group(['middleware' => ['auth.jwt']], function () {
        Route::get('hello-world', function () {
            return 'Hello World';
        });
	});
});

?>
```

## Define relations on external models and serializers
Create a `RelationsServiceProvider` and register it inside your `ModuleServiceProvider`.
Inside the `boot`-method of your `RelationsServiceProvider` you can define
relations on external models and serializers as follows:
```
public function boot()
{
    config([
        'runtimeRelations.models.'.\Netmon\Server\App\Models\User::class => [
            'devices' => [
                'type' => 'hasMany',
                'model' => \Netmon\Devices\Models\Device::class,
                'foreignKey' => 'owner_id',
                'localKey' => null
            ]
        ]
    ]);

    config([
        'runtimeRelations.serializers.'.\Netmon\Server\App\Serializers\UserSerializer::class => [
            'devices' => [
                'type' => 'hasMany',
                'serializer' => \Netmon\Devices\Serializers\DeviceSerializer::class
            ]
        ]
    ]);

    parent::boot();
}
```

These relations will be loaded at runtime.

## Test our new module
Thats it. Now we should be good to install our module and check out
our first route available at http://localhost:8000/hello-world
