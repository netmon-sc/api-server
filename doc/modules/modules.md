# Modules
Modules are the prefered way to extend the Netmon server. This is the user
manual for modules. Developers please see
[new module development manual](./development_new_module.md).


## Module list
Devices
 * Repository: https://git.ffnw.de/netmon-sc/api-devices

More modules will be available at [packagist](https://packagist.org/packages/netmon-server/).

## Installing a module in general
All modules listed at https://packagist.org/packages/netmon-server/ are already
in your project folder and do not need to be fetched. For modules not in
the mentioned list please require them using composer first:
```
composer require <VENDOR>/<MODULENAME>
```

All modules need to be installed and configured:
```
php artisan module:install <MODULENAME> "Netmon\<MODULENAME>\ModuleServiceProvider"
php artisan module:configure <MODULENAME>
```
