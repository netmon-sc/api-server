# Unittesting
Unit testing is done using [phpunit](https://phpunit.de/). All tests can be
found in the [tests](../tests) folder (see this folder for examples!). Phpunit is configured using
the [phpunit.xml](../phpunit.xml) file. This file is used to configure phpunit to
to use the `testing` environment while running. This makes netmon use the
[.env.testing](../.env.testing) configuration file while running phpunit.

By default netmon uses an in-memory sqlite database for testing. To run the
tests just install phpunit, enable the sqlite php extension and run the
command `phpunit` from inside the [root](../) folder.

References
 * https://laravel.com/docs/5.2/testing
 * https://laravel.com/api/5.2/Illuminate/Foundation/Testing/TestCase.html
 * https://laravel.com/docs/5.2/configuration#environment-configuration

## Basic test classes
For make writing of unit tests easier we have three basic test classes
to inherit from:
 * **tests/BasicTestCase.php:** Use this for verry basic testcase
 * **tests/ResourceTestCase.php:** Use this for testing api resources and collections that do not need the basic database setup (i.e. the unit test for creating the first user uses this testcase)
 * **tests/ResourceTestCase.php:** Use this for testing normal resources and collections that need some users and roles setup by default. Usually you want to inherit from this testcase.

## Writing tests
### Testing JSON resources
First add basic structure information to your testcase
```
public function getStructure() {
    return [
        'type',
        'attributes' => [
            'name',
            'email'
        ]
    ];
}
```

Second add your first testing method (note: the name of methods used for unittesting
need to start with `test`, all other methods will be ignored):
```
public function testFirstUser() {
    //create first user
    $this->json('POST', '/users', [
        'data' => [
            'type' => "user",
            'attributes' => [
                'name' => "firstuser",
                'password' => "testtest"
            ]
        ]
    ]);
    //check if response content is json and machted matches the structure
    //definition of this resource
    $this->seeJsonStructure($this->getResourceStrucure());
    //check if the statuscode is the expected one
    $this->assertResponseStatus(201);
}
```

### Adding some special headers to your response
```
//https://stackoverflow.com/questions/30772207/phpunit-and-http-content-type#comment49667338_30772323
    $response = $this->call(
    'POST',
    'http://localhost/users',
    [],
    [],
    [],
    ['CONTENT_TYPE' => 'application/json'],
    json_encode($content, true)
);
$this->assertEquals(201, $response->getStatusCode());
$this->assertEquals('application/json', $response->headers->get('Content-Type'));
```
